package group2.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.GooglePojo;

public class UserLoginGoogleRequest {

	@JsonProperty("firstName")
	private String firstName;
	
	@JsonProperty("lastName")
	private String lastName;
	
	@JsonProperty("userName")
	private String userName;

	@JsonProperty("email")
	private String email;
	
	@JsonProperty("role_name")
	private String roleName;
	

	public UserLoginGoogleRequest() {
		
	}
	public UserLoginGoogleRequest(GooglePojo user) {
		this.firstName= user.getFamily_name();
		this.lastName = user.getGiven_name();
		this.email = user.getEmail();
		this.roleName="ROLE_USER";
		this.userName = user.getName();
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	
}
