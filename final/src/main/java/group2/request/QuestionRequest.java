package group2.request;

import java.util.ArrayList;
import java.util.List;

public class QuestionRequest {
	private int id;
	
	private String content;
	
	private int type;
	
	private List<AnswerRequest> answers = new ArrayList<AnswerRequest>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<AnswerRequest> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerRequest> answers) {
		this.answers = answers;
	}


}
