package group2.request;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class QuizRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String quizName;
	private String tagName;
	private MultipartFile image;

	public String getQuizName() {
		return quizName;
	}

	public void setQuizName(String quizName) {
		this.quizName = quizName;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

}
