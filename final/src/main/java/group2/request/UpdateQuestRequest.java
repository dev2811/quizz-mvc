package group2.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateQuestRequest {

	@JsonProperty("id")
	private int id;
	@JsonProperty("question")
	private String questContent;
	@JsonProperty("answer")
	private String answer;
	@JsonProperty("list_fake")
	private List<String> listFake;
	@JsonProperty("list_real")
	private List<String> listReal;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestContent() {
		return questContent;
	}
	public void setQuestContent(String questContent) {
		this.questContent = questContent;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public List<String> getListFake() {
		return listFake;
	}
	public void setListFake(List<String> listFake) {
		this.listFake = listFake;
	}
	public List<String> getListReal() {
		return listReal;
	}
	public void setListReal(List<String> listReal) {
		this.listReal = listReal;
	}
	
	
}
