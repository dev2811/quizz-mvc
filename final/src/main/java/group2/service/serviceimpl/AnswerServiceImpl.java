package group2.service.serviceimpl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import group2.response.AnswerResponse;
import group2.response.BaseResponse;
import group2.service.AnswerService;

public class AnswerServiceImpl implements AnswerService{
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public boolean createAnswer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<AnswerResponse> getAnswerForQuestionId(int questId) {
		String ROOT_URI = "http://localhost:8082/api/answer/get-answer-by-questId?id="+questId;
		try {
			BaseResponse response = restTemplate.getForObject(ROOT_URI, BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			AnswerResponse[] answerResponses = mapper.convertValue(response.getData(), AnswerResponse[].class);
			return Arrays.asList(answerResponses);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
