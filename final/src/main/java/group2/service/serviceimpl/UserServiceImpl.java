
package group2.service.serviceimpl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import group2.config.RestTemplateConfig;
import group2.request.UpdateQuestRequest;
import group2.request.UserLoginGoogleRequest;
import group2.request.UserLoginRequest;
import group2.response.BaseResponse;
import group2.response.LoginedUserResponse;
import group2.response.PostResponse;
import group2.response.QuizzResponse;
import group2.response.UserLoginResponse;
import group2.response.UserPaginationResponse;
import group2.response.UserResponse;
import group2.response.UserSearchResponse;
import group2.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	RestTemplateConfig restTe;
	
	@Autowired
	private RestTemplate restTemplate;

	public List<PostResponse> getAllPerson() {
//		final String ROOT_URI = "http://localhost:8084/api/post/list/all";
//		ResponseEntity<PostResponse[]> response = restTemplate.getForEntity(ROOT_URI, PostResponse[].class);
//		return Arrays.asList(response.getBody());
		return null;

	}

	@Override
	public UserLoginResponse checkLogin(UserLoginRequest user) {
		String ROOT_URI = "http://localhost:8082/api/user/login";
		restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    JSONObject userJsonObject = new JSONObject();
	    userJsonObject.put("user_name", user.getUsername());
	    userJsonObject.put("password", user.getPassword());
		HttpEntity<String> request = 
			      new HttpEntity<String>(userJsonObject.toString(), headers);
		BaseResponse response = restTemplate.postForObject(ROOT_URI, request, BaseResponse.class);
		
		ObjectMapper mapper = new ObjectMapper();
		
		UserLoginResponse userResponse = mapper.convertValue(response.getData(), UserLoginResponse.class);
		return userResponse;
	}

	@Override
	public List<UserResponse> getListForRank(int page) {
		String ROOT_URI = "http://localhost:8082/api/user/rank?page="+page;
		try {
			BaseResponse response = restTemplate.getForObject(ROOT_URI, BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			UserResponse[] userResponse = mapper.convertValue(response.getData(), UserResponse[].class);
			return Arrays.asList(userResponse);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int countAllListForRank() {
		String ROOT_URI = "http://localhost:8082/api/user/listOfAllForRank";
		try {
			BaseResponse response = restTemplate.getForObject(ROOT_URI, BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			UserResponse[] userResponse = mapper.convertValue(response.getData(), UserResponse[].class);
			return Arrays.asList(userResponse).size();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override

	public UserLoginResponse checkLoginGoogle(UserLoginGoogleRequest userRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/login-google";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    HttpEntity<UserLoginGoogleRequest> request = new HttpEntity<>(userRequest, headers);
	    ResponseEntity<BaseResponse> response = restTemplate.postForEntity(ROOT_URI, request, BaseResponse.class);
		
	    ObjectMapper mapper = new ObjectMapper();
		UserLoginResponse userResponse = mapper.convertValue(response.getBody().getData(), UserLoginResponse.class);
		return userResponse;
	}
	
	public UserPaginationResponse getAllUsers(int pageNumber, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/get-all-users?pageNumber="+pageNumber;
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//get all users
		ResponseEntity<UserPaginationResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, UserPaginationResponse.class);
		
		return response.getBody();
	}

	@Override
	public UserSearchResponse getUserByUsername(String username, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/get-user-by-name?username="+username;
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//get users by username
		ResponseEntity<UserSearchResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, UserSearchResponse.class);
		
		return response.getBody();
	}

	@Override
	public UserSearchResponse getUserByStatus(int status, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/get-user-by-status?status="+status;
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//get user by status
		ResponseEntity<UserSearchResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, UserSearchResponse.class);
		
		return response.getBody();
	}

	@Override
	public LoginedUserResponse disableUser(int id, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/disable-user/"+id;
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//disable user
		ResponseEntity<LoginedUserResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, LoginedUserResponse.class);
		
		return response.getBody();
	}
	
	@Override
	public LoginedUserResponse activateUser(int id, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/user/activate-user/"+id;
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//disable user
		ResponseEntity<LoginedUserResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, LoginedUserResponse.class);
		
		return response.getBody();
	}

}
