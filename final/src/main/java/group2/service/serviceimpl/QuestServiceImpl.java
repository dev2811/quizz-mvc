package group2.service.serviceimpl;

import java.util.Collections;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import group2.request.QuestRequest;
import group2.request.QuestionListRequest;
import group2.request.UpdateQuestRequest;
import group2.response.BaseResponse;
import group2.response.QuestionListResponse;
import group2.response.QuestionResponse;
import group2.response.ResultResponse;
import group2.response.TotalAndNameResponse;
import group2.response.UserLoginResponse;
import group2.response.QuestGeneralResponse;
import group2.response.QuestPaginationResponse;
import group2.response.QuestResponse;
import group2.service.QuestService;

@Service
public class QuestServiceImpl implements QuestService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public int createQuest(QuestRequest quest,String token) {
		String ROOT_UIL = "http://localhost:8082/api/quest/add";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<QuestRequest> request = new HttpEntity<>(quest, headers);
		ResponseEntity<BaseResponse> response = restTemplate.postForEntity(ROOT_UIL, request, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			return 1;
		} else if (response.getBody().getStatus() == 400) {
			return 2;
		}
		return 3;
	}

	@Override
	public boolean updateQuest(UpdateQuestRequest quest,String token) {
		String ROOT_UIL = "http://localhost:8082/api/quest/update";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		HttpEntity<UpdateQuestRequest> request = new HttpEntity<>(quest, headers);
		ResponseEntity<BaseResponse> response = restTemplate.postForEntity(ROOT_UIL, request, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			return true;
		}
		return false;
	}

	@Override
	public List<QuestionResponse> getAllQuestFromQuiz(int id, String token) {
		String ROOT_URI = "http://localhost:8082/api/quest/get-question-by-quizId?id=" + id;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("authorization", token);
			HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
			ResponseEntity<BaseResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity,
					BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			QuestionListResponse questionResponses = mapper.convertValue(response.getBody().getData(),
					QuestionListResponse.class);
			if (questionResponses != null) {
				return questionResponses.getQuestions();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public QuestPaginationResponse getAllQuestPagination(int quizId, int pageNumber, int pageLimit) {
		String ROOT_UIL = "http://localhost:8082/api/quiz/" + quizId + "/get-quests?page-number=" + pageNumber
				+ "&page-limit=" + pageLimit;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		ResponseEntity<BaseResponse> response = restTemplate.getForEntity(ROOT_UIL, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			Gson g = new Gson();
			QuestPaginationResponse quests = new QuestPaginationResponse();
			String json = g.toJson(response.getBody().getData());
			quests = g.fromJson(json, QuestPaginationResponse.class);
			return quests;

		}
		return null;
	}

	@Override
	public TotalAndNameResponse getTotalQuestionAndQuizName(int id, String token) {
		String ROOT_URI = "http://localhost:8082/api/quest/get-total-question-and-quiz-name?quizId=" + id;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("authorization", token);
			HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
			ResponseEntity<BaseResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity,
					BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			TotalAndNameResponse questionResponses = mapper.convertValue(response.getBody().getData(),
					TotalAndNameResponse.class);
			return questionResponses;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public QuestResponse getGetDetail(int questId) {
		String ROOT_UIL = "http://localhost:8082/api/quest/"+questId+"/detail";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		ResponseEntity<BaseResponse> response = restTemplate.getForEntity(ROOT_UIL, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			Gson g = new Gson();
			QuestResponse quests = new QuestResponse();
			String json = g.toJson(response.getBody().getData());
			quests = g.fromJson(json,QuestResponse.class);
			return quests;
		}
		return null;
	}

	@Override
	public ResultResponse getResultQuestion(QuestionListRequest quest, int quizId, String token, String userName) {
		String ROOT_URI = "http://localhost:8082/api/quest/result?username="+userName+"&quizId="+quizId;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
			headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			headers.set("authorization", token);
			HttpEntity<QuestionListRequest> request = new HttpEntity<>(quest, headers);
			BaseResponse response = restTemplate.postForObject(ROOT_URI, request, BaseResponse.class);
			ObjectMapper mapper = new ObjectMapper();
			ResultResponse questionResponses = mapper.convertValue(response.getData(), ResultResponse.class);
			return questionResponses;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean deleteQuest(int id,String token   ) {
		String ROOT_UIL = "http://localhost:8082/api/quest/" + id + "/delete";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		ResponseEntity<BaseResponse> response = restTemplate.getForEntity(ROOT_UIL, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			return true;
		}
		return false;
	}

	@Override
	public QuestPaginationResponse getAllQuestPaginationByName(int quizId, String content) {
		String ROOT_UIL = "http://localhost:8082/api/quiz/"+quizId+"/get-quests-by-name?text-search="+content;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		ResponseEntity<BaseResponse> response = restTemplate.getForEntity(ROOT_UIL, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			Gson g = new Gson();
			QuestPaginationResponse quests = new QuestPaginationResponse();
			String json = g.toJson(response.getBody().getData());
			quests = g.fromJson(json,QuestPaginationResponse.class);
			return quests;
		}
		return null;
	}
}
