package group2.service.serviceimpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import group2.response.BaseResponse;
import group2.response.LoginedUserResponse;
import group2.response.QuestionResponse;
import group2.response.QuizzResponse;
import group2.response.QuizzResponseWithOnlyData;
import group2.service.QuizzService;

@Service
public class QuizzServiceImpl implements QuizzService {
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public QuizzResponse getAllQuizz(int pageNumber, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/home/?pageNumber=" + pageNumber;

		// set Authorization token in header http
		String tokenString = tokenRequest;
		// String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);

		// get list quizz
		ResponseEntity<QuizzResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity,
				QuizzResponse.class);

		return response.getBody();
	}

	@Override
	public QuizzResponseWithOnlyData getQuizzByName(String name, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/home/get-quiz-by-name?name=" + name;

		// set Authorization token in header http
		String tokenString = tokenRequest;
		// String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);

		// get list quizz
		ResponseEntity<QuizzResponseWithOnlyData> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity,
				QuizzResponseWithOnlyData.class);

		return response.getBody();
	}

	@Override
	public boolean deleteQuizz(int id, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/quiz/delete/" + id;

		// set Authorization token in header http
		String tokenString = tokenRequest;
		// String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);

		// delete quiz
		ResponseEntity<QuizzResponseWithOnlyData> response = restTemplate.exchange(ROOT_URI, HttpMethod.DELETE,
				jwtEntity, QuizzResponseWithOnlyData.class);
		QuizzResponseWithOnlyData dataBody = response.getBody();
		if (dataBody.getStatus() == 200) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public LoginedUserResponse getLoginedUser(String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/home/get-logined-user";

		// set Authorization token in header http
		String tokenString = tokenRequest;
		// String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);

		// get logined user
		ResponseEntity<LoginedUserResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity,
				LoginedUserResponse.class);

		return response.getBody();
	}

@Override
	public int createQuiz(String quizName, String tagName,String userName,String token,String urlImage) {
		String ROOT_UIL="http://localhost:8082/api/quiz/add";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("authorization", token);
		Map<String, Object> map = new HashMap<>();
		map.put("quiz_name", quizName);
		map.put("tag_name", tagName);
		map.put("user_name", userName);
		map.put("image", urlImage);
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
		ResponseEntity<BaseResponse> response = restTemplate.postForEntity(ROOT_UIL, entity, BaseResponse.class);
		if (response.getBody().getStatus() == 200) {
			return 1;
		} else if (response.getBody().getStatus() == 400) {
			return 2;
		}
		return 3;
	}

	@Override
	public List<QuestionResponse> getQuestionAndAnswerForQuiz(int quiz) {
		// TODO Auto-generated method stub
		return null;
	}

}
