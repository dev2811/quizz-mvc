package group2.service.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import group2.config.RestTemplateConfig;
import group2.response.FacebookUserResponse;
import group2.response.LoginedUserResponse;
import group2.response.QuizzResponse;
import group2.service.SocialLoginService;

@Service
public class SocialLoginServiceImpl implements SocialLoginService {
	
	@Autowired
	RestTemplateConfig restTe;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public FacebookUserResponse getLoginFacebookUser(String facebookToken) {
		String ROOT_URI = "http://localhost:8082/api/oauth/facebook";
		
		//set Authorization token in header http
		//String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		//headers.set("authorization", tokenString);
		
		//set body
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("facebookToken", facebookToken);
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		
		//get facebook logined user
		ResponseEntity<FacebookUserResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.POST, request, FacebookUserResponse.class);

		return response.getBody();
	}

}
