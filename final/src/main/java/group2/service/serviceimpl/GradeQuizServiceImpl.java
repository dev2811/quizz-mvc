package group2.service.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import group2.response.GradeQuizResponse;
import group2.response.QuizzResponse;
import group2.service.GradeQuizService;

@Service
public class GradeQuizServiceImpl implements GradeQuizService {
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public GradeQuizResponse getAllGradeQuizOfUser(String username, String tokenRequest) {
		String ROOT_URI = "http://localhost:8082/api/gradequiz/get-grade-quiz-of-user/";
		
		//set Authorization token in header http
		String tokenString = tokenRequest;
		//String token = "Bearer " + tokenString;
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("authorization", tokenString);
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		
		//get list quizz
		ResponseEntity<GradeQuizResponse> response = restTemplate.exchange(ROOT_URI, HttpMethod.GET, jwtEntity, GradeQuizResponse.class);
	
		return response.getBody();
	}

}
