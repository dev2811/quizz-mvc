package group2.service;

import java.util.List;

import group2.response.GradeQuizResponse;

public interface GradeQuizService {
	GradeQuizResponse getAllGradeQuizOfUser(String username, String tokenRequest);
}
