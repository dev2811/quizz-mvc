package group2.service;

import java.util.List;

import group2.request.QuestRequest;
import group2.request.QuestionListRequest;
import group2.request.UpdateQuestRequest;
import group2.response.QuestionResponse;
import group2.response.ResultResponse;
import group2.response.TotalAndNameResponse;
import group2.response.QuestPaginationResponse;
import group2.response.QuestResponse;

public interface QuestService{

	public int createQuest(QuestRequest quest,String token);
	
	public boolean updateQuest(UpdateQuestRequest quest,String token);

	public List<QuestionResponse> getAllQuestFromQuiz(int id, String token);
	
	public TotalAndNameResponse getTotalQuestionAndQuizName(int id, String token);
	
	public ResultResponse getResultQuestion(QuestionListRequest request, int quizId, String token, String userName);

	public QuestPaginationResponse getAllQuestPagination(int quizId,int pageNumber,int pageLimit);
	
	public QuestPaginationResponse getAllQuestPaginationByName(int quizId,String content);
	
	public QuestResponse getGetDetail(int questId);
	
	public boolean deleteQuest(int id,String token);
}
