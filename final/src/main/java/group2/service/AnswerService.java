package group2.service;

import java.util.List;

import group2.response.AnswerResponse;

public interface AnswerService {

	public boolean createAnswer();
	
	public List<AnswerResponse> getAnswerForQuestionId(int questId);
}
