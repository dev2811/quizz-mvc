package group2.service;

import java.util.List;

import group2.request.UserLoginGoogleRequest;
import group2.request.UserLoginRequest;
import group2.response.LoginedUserResponse;
import group2.response.PostResponse;
import group2.response.UserLoginResponse;
import group2.response.UserPaginationResponse;
import group2.response.UserResponse;
import group2.response.UserSearchResponse;

public interface UserService {

	public List<PostResponse> getAllPerson();
	
	public UserLoginResponse checkLogin(UserLoginRequest user);
	
	public List<UserResponse> getListForRank(int page);
	
	public int countAllListForRank();
	
	public UserLoginResponse checkLoginGoogle(UserLoginGoogleRequest userRequest);

	public UserPaginationResponse getAllUsers(int pageNumber, String tokenRequest);
	
	public UserSearchResponse getUserByUsername(String username, String tokenRequest);
	
	public UserSearchResponse getUserByStatus(int status, String tokenRequest);
	
	public LoginedUserResponse disableUser(int id, String tokenRequest);
	
	public LoginedUserResponse activateUser(int id, String tokenRequest);

}
