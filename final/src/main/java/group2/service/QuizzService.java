package group2.service;

import java.util.List;

import group2.dto.Quizz;
import group2.response.LoginedUserResponse;
import group2.response.QuestionResponse;
import group2.response.QuizzResponse;
import group2.response.QuizzResponseWithOnlyData;
import group2.response.UserResponse;

public interface QuizzService {
	QuizzResponse getAllQuizz(int pageNumber, String tokenRequest);

	QuizzResponseWithOnlyData getQuizzByName(String name, String tokenRequest);

	boolean deleteQuizz(int id, String tokenRequest);

	LoginedUserResponse getLoginedUser(String tokenRequest);

	List<QuestionResponse> getQuestionAndAnswerForQuiz(int quiz);

	int createQuiz(String quizName,String tagName,String userName,String token,String urlImage);
}
