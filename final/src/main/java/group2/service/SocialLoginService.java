package group2.service;

import group2.response.FacebookUserResponse;

public interface SocialLoginService {
	public FacebookUserResponse getLoginFacebookUser(String facebookToken);
}
