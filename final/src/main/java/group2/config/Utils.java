package group2.config;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

public class Utils {

	private UploadImageConfig upLoadImage = new UploadImageConfig();

	public static int getPagina(int total, int limit) {
		double totalD = (double) total;
		double limitD = (double) limit;
		if ((totalD / limitD) - (double) (total / limit) > 0) {
			return (total / limit) + 1;
		}
		return total / limit;
	}

	public String getImageUrl(MultipartFile imageMulti) {
		Cloudinary cloudinary = upLoadImage.cloudinary();
		try {
			Map avatarLink = cloudinary.uploader().upload(imageMulti.getBytes(),
					ObjectUtils.asMap("resource_type", "auto"));
			return (String) avatarLink.get("secure_url");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getImageDefaut() {
		Random r = new Random();
		int index = r.nextInt(3);
		if (index == 0) {
			return "https://res.cloudinary.com/dfv78gfoi/image/upload/v1637426264/x08xal4hyx3makp4usse.png";
		}
		if (index == 1) {
			return "https://res.cloudinary.com/dfv78gfoi/image/upload/v1637437128/w4btcktgzztccqgnhhzl.png";
		}
		return "https://res.cloudinary.com/dfv78gfoi/image/upload/v1637597721/e6hzzz0rhhcpzhrjygya.jpg";
	}

}
