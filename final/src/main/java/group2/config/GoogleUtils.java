package group2.config;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import group2.dto.GooglePojo;
import group2.response.QuizzResponse;

@Component
public class GoogleUtils {

	@Autowired
	private RestTemplate restTemplate;

	public static String GOOGLE_CLIENT_ID = "949786020-he0779607ab4ek3p21mmol766ltjct2h.apps.googleusercontent.com";
	public static String GOOGLE_CLIENT_SECRET = "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";
	public static String GOOGLE_REDIRECT_URI = "http://localhost:8083/final/login-google";
	public static String GOOGLE_LINK_GET_TOKEN = "https://accounts.google.com/o/oauth2/token";
	public static String GOOGLE_LINK_GET_USER_INFO = "https://www.googleapis.com/oauth2/v2/userinfo?access_token=";
	public static String GOOGLE_GRANT_TYPE = "authorization_code";

	public String getToken(final String code) throws ClientProtocolException, IOException {
		String response = Request.Post(GOOGLE_LINK_GET_TOKEN)
				.bodyForm(Form.form().add("client_id", GOOGLE_CLIENT_ID).add("client_secret", GOOGLE_CLIENT_SECRET)
						.add("redirect_uri", GOOGLE_REDIRECT_URI).add("code", code).add("grant_type", GOOGLE_GRANT_TYPE)
						.build())
				.execute().returnContent().asString();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(response).get("access_token");
		return node.textValue();
	}


	public GooglePojo getUserInfo(String token) {
		String links = "https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token;
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> jwtEntity = new HttpEntity<String>(headers);
		ResponseEntity<GooglePojo> response = restTemplate.exchange(links, HttpMethod.GET, jwtEntity,GooglePojo.class);
		GooglePojo userInfor =response.getBody();
		return userInfor;
	}
}
