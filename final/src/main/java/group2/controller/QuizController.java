package group2.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Util;

import group2.config.Utils;
import group2.request.QuizRequest;
import group2.response.LoginedUserResponse;
import group2.response.QuestGeneralResponse;
import group2.response.QuestPaginationResponse;
import group2.response.QuizzAddResponse;
import group2.service.QuestService;
import group2.service.QuizzService;

@Controller
public class QuizController {

	@Autowired
	private QuizzService quizService;

	@Autowired
	private QuestService questSerice;

	@RequestMapping(value = "/add-quiz", method = RequestMethod.POST)
	public @ResponseBody QuizzAddResponse addQuiz(@ModelAttribute QuizRequest wrapper, HttpSession session) {
		QuizzAddResponse response = new QuizzAddResponse();
		String urlImage = new Utils().getImageUrl(wrapper.getImage());
		String userName = (String) session.getAttribute("userName");
		String token = (String) session.getAttribute("token");
		response.setStatus(quizService.createQuiz(wrapper.getQuizName(), wrapper.getTagName(), userName, token,
				wrapper.getQuizName() != null ? urlImage : new Utils().getImageDefaut()));
		return response;
	}

	@RequestMapping(value = "/quiz", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getIndex(Model model, HttpSession session) {
		QuizRequest quizz = new QuizRequest();
		model.addAttribute("quizz", quizz);
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser", loginedUser.getUser());
		return "createquiz";
	}

	@RequestMapping(value = "/{id}/get-all-quest", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public String getQuestPanagi(Model model, @PathVariable("id") int quizId,
			@RequestParam(name = "page-number", required = false, defaultValue = "0") int page,
			@RequestParam(name = "page-limit", required = false, defaultValue = "6") int limit,
			@RequestParam(name = "quizz-name", required = false, defaultValue = "No Name") String quizName,
			HttpSession session) {
		QuestPaginationResponse listQuest = questSerice.getAllQuestPagination(quizId, page, limit);
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser", loginedUser.getUser());
		model.addAttribute("listQuest", listQuest.getQuests());
		model.addAttribute("total", listQuest.getTotal());
		model.addAttribute("pagina", Utils.getPagina(listQuest.getTotal(), limit));
		model.addAttribute("page", page);
		model.addAttribute("quizzName", quizName);
		model.addAttribute("quizzId", quizId);
		return "managequest";
	}

	@RequestMapping(value = "/{id}/get-all-quest-by-name", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public String getQuestPanagiByName(Model model, @PathVariable("id") int quizId,
			@RequestParam(name = "text-search", required = false, defaultValue = "") String content,
			@RequestParam(name = "page-number", required = false, defaultValue = "0") int page,
			@RequestParam(name = "page-limit", required = false, defaultValue = "100") int limit,
			@RequestParam(name = "quizz-name", required = false, defaultValue = "Hi Name") String quizName,
			HttpSession session) {
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser", loginedUser.getUser());
		QuestPaginationResponse listQuest = questSerice.getAllQuestPaginationByName(quizId, content);
		model.addAttribute("listQuest", listQuest.getQuests());
		model.addAttribute("total", listQuest.getTotal());
		model.addAttribute("pagina", Utils.getPagina(listQuest.getTotal(), limit));
		model.addAttribute("page", page);
		model.addAttribute("quizzName", quizName);
		model.addAttribute("quizzId", quizId);
		return "managequest";
	}
}
