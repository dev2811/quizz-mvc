package group2.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import group2.config.GoogleUtils;
import group2.dto.GooglePojo;
import group2.request.UserLoginGoogleRequest;
import group2.request.UserLoginRequest;
import group2.response.LoginedUserResponse;
import group2.response.UserLoginResponse;
import group2.response.UserResponse;
import group2.service.QuizzService;
import group2.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;

	@Autowired
	private GoogleUtils googleUtils;
	
	@Autowired
	private QuizzService quizzService;

	@RequestMapping("/")
	public String getLoginPage(Model model) {
		UserLoginRequest user = new UserLoginRequest();
		model.addAttribute("user", user);
		return "login";
	}

	@RequestMapping("/register")
	public String getRegisterPage() {
		return "register";
	}

	@RequestMapping("/rank")
	public String getRankPage(@RequestParam(defaultValue = "0") int page, Model model, HttpSession session) {
		// get token from session
		String token = (String) session.getAttribute("token");
		if (page != 0) {
			page--;
		}
		List<UserResponse> userResponse = userService.getListForRank(page);
		model.addAttribute("users", userResponse);
		int totalItem = userService.countAllListForRank();
		int totalPage = (int) Math.ceil((double) totalItem / 10);
		LoginedUserResponse loginedUser = quizzService.getLoginedUser(token);
		model.addAttribute("loginedUser", loginedUser.getUser());
		model.addAttribute("page", ++page);
		model.addAttribute("totalItem", totalItem);
		model.addAttribute("totalPage", totalPage);
		return "rank";
	}

	@RequestMapping("/checkLogin")
	// @ResponseBody
	public String getCheckLogin(Model model, @ModelAttribute("user") UserLoginRequest user, BindingResult bindingResult,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		UserLoginResponse userResponse = userService.checkLogin(user);
		if (userResponse != null) {
			session.setAttribute("userName", userResponse.getName());
			session.setAttribute("token", userResponse.getToken());
			// System.out.println(session.getAttribute("token"));
			if (userResponse.getRole().equalsIgnoreCase("ROLE_USER")) {
				return "redirect:/index";
			} else {
				return "redirect:/get-manage-quiz-page";
			}
		}
		model.addAttribute("errMsg", "Username/Password is wrong");
		return "login";
	}

	@RequestMapping(value = "/login-google", method = RequestMethod.GET)
	public String loginGoogle(HttpServletRequest request) throws ClientProtocolException, IOException {
		GooglePojo googlePojo = googleUtils.getUserInfo(googleUtils.getToken(request.getParameter("code")));
		UserLoginGoogleRequest userRequest = new UserLoginGoogleRequest(googlePojo);
		UserLoginResponse userResponse = userService.checkLoginGoogle(userRequest);
		HttpSession session = request.getSession();
		if (userResponse != null) {
			session.setAttribute("userName", userResponse.getName());
			// session.setAttribute("role", userResponse.getRole());
			session.setAttribute("token", userResponse.getToken());
			// System.out.println(session.getAttribute("token"));
		}
		if (userResponse.getRole().equalsIgnoreCase("ROLE_USER")) {
			return "redirect:/index";
		} else {
			return "redirect:/get-manage-quiz-page";
		}
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("token", null);
		session.setAttribute("userName", null);
		return "redirect:/";

	}
}
