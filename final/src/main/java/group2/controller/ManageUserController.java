package group2.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.dto.Quizz;
import group2.dto.User;
import group2.response.LoginedUserResponse;
import group2.response.QuizzResponse;
import group2.response.QuizzResponseWithOnlyData;
import group2.response.UserPaginationResponse;
import group2.response.UserSearchResponse;
import group2.service.QuizzService;
import group2.service.UserService;

@Controller
public class ManageUserController {
	@Autowired
	private QuizzService quizzService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/get-manage-user-page", method=RequestMethod.GET)
	public String getManageUserPage(Model model, @RequestParam(defaultValue = "1") int pageNumber, HttpSession session) {
		String token = (String) session.getAttribute("token");
		
		//get users
		UserPaginationResponse userResponse = userService.getAllUsers(pageNumber, token);
		model.addAttribute("users",userResponse.getUsers().getUsers());
		model.addAttribute("totalPages", userResponse.getUsers().getTotalPages());
		
		LoginedUserResponse loginedUser = quizzService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		
		return "manageuser";
	}
	
	@RequestMapping(value = "/get-users-by-page-number", method = RequestMethod.GET)
	public @ResponseBody List<User> getUsersByPageNumber(@RequestParam(defaultValue = "1") int pageNumber, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		UserPaginationResponse userResponse = userService.getAllUsers(pageNumber, token);
		return userResponse.getUsers().getUsers();
	}
	
	@RequestMapping(value = "/get-users-by-username", method = RequestMethod.GET)
	public @ResponseBody List<User> getUserByUsername(@RequestParam(defaultValue = "") String username, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		UserSearchResponse userResponse = userService.getUserByUsername(username, token);
		return userResponse.getUsers();
	}
	
	@RequestMapping(value = "/get-users-by-status", method = RequestMethod.GET)
	public @ResponseBody List<User> getUserByStatus(@RequestParam(defaultValue = "-1") int status, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		UserSearchResponse userResponse = userService.getUserByStatus(status, token);
		return userResponse.getUsers();
	}
	
	@RequestMapping(value = "/disable-user/{id}", method = RequestMethod.GET)
	public @ResponseBody User disableUser(@PathVariable int id, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		LoginedUserResponse userResponse = userService.disableUser(id, token);
		return userResponse.getUser();
	}
	
	@RequestMapping(value = "/activate-user/{id}", method = RequestMethod.GET)
	public @ResponseBody User activateUser(@PathVariable int id, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		LoginedUserResponse userResponse = userService.activateUser(id, token);
		return userResponse.getUser();
	}
}
