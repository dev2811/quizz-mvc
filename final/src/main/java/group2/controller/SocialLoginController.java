package group2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.dto.FacebookUser;
import group2.response.FacebookUserResponse;
import group2.response.LoginedUserResponse;
import group2.service.SocialLoginService;

@Controller
@RequestMapping("/oauth")
public class SocialLoginController {
	@Autowired
	private SocialLoginService socialLoginService;
	
	//login with facebook
	@RequestMapping(value = "/facebook",method = RequestMethod.POST,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public @ResponseBody FacebookUserResponse loginWithFacebook(@RequestBody MultiValueMap<String, String> formData, HttpServletRequest request) {
		//System.out.println(formData.get("facebookToken").get(0));
		HttpSession session = request.getSession();
		FacebookUserResponse userResponse = socialLoginService.getLoginFacebookUser(formData.get("facebookToken").get(0));
		
		//if res != null => redirect:/checkLogin, else forward:/ model.addAttribute
		if(userResponse.getUser() != null) {
			session.setAttribute("token", userResponse.getUser().getToken());
			session.setAttribute("userName", userResponse.getUser().getUserName());
		}
		
		return userResponse;
	}
}
