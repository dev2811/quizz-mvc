package group2.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.response.LoginedUserResponse;
import group2.response.QuizzResponse;
import group2.response.QuizzResponseWithOnlyData;
import group2.service.QuizzService;

@Controller
public class ManageQuizController {
	@Autowired
	QuizzService quizzService;
	
	@RequestMapping(value = "/get-manage-quiz-page", method = RequestMethod.GET)
	public String getManageQuizPage(Model model, @RequestParam(defaultValue = "1") int pageNumber,HttpSession session) {
		String token = (String) session.getAttribute("token");
		
		QuizzResponse quizzResponse = quizzService.getAllQuizz(pageNumber, token);
		model.addAttribute("quizzes",quizzResponse.getQuizzPagination().getQuizes());
		model.addAttribute("totalPages", quizzResponse.getQuizzPagination().getTotalPages());
		
		LoginedUserResponse loginedUser = quizzService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		
		return "managequiz";
	}
	
	@RequestMapping(value = "/delete-quiz/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Object> deleteQuiz(@PathVariable int id, HttpSession session) {
		String token = (String) session.getAttribute("token");
		
		boolean check = quizzService.deleteQuizz(id, token);
		if(check) {
			return new ResponseEntity<Object>(null, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
		}
	}
}
