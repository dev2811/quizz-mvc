package group2.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.dto.Quizz;
import group2.response.LoginedUserResponse;
import group2.response.PostResponse;
import group2.response.QuizzResponse;
import group2.response.QuizzResponseWithOnlyData;
import group2.response.UserResponse;
import group2.service.QuizzService;
import group2.service.UserService;

@Controller
public class HomeController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private QuizzService quizzService;
	
	@RequestMapping(value="/index",  method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getIndex(Model model, @RequestParam(defaultValue = "1") int pageNumber,HttpSession session) {
		//get token from session
		String token = (String) session.getAttribute("token");
		
		QuizzResponse quizzResponse = quizzService.getAllQuizz(pageNumber, token);
		model.addAttribute("quizzes",quizzResponse.getQuizzPagination().getQuizes());
		model.addAttribute("totalPages", quizzResponse.getQuizzPagination().getTotalPages());
		
		LoginedUserResponse loginedUser = quizzService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		
		return "index";
	}
//	@RequestMapping("/")
//	public String getLoginPage() {
//		List<PostResponse> list = userService.getAllPerson();
//		return "home";
//	}
	
	@RequestMapping(value = "/get-quizzes-by-page-number", method = RequestMethod.GET)
	public @ResponseBody List<Quizz> getQuizzesByPageNumber(@RequestParam(defaultValue = "1") int pageNumber, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		QuizzResponse quizzResponse = quizzService.getAllQuizz(pageNumber, token);
		return quizzResponse.getQuizzPagination().getQuizes();
	}
	
	@RequestMapping(value = "/get-quizzes-by-name", method = RequestMethod.GET)
	public @ResponseBody List<Quizz> getQuizzByName(@RequestParam(defaultValue = "") String name, HttpSession session){
		String token = (String) session.getAttribute("token");
		
		QuizzResponseWithOnlyData quizzSearchByNameResponse = quizzService.getQuizzByName(name, token);
		return quizzSearchByNameResponse.getQuizzes().getQuizes();
	}
}
