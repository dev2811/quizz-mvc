package group2.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.response.GradeQuizResponse;
import group2.response.LoginedUserResponse;
import group2.service.GradeQuizService;
import group2.service.QuizzService;

@Controller
public class HistoryController {
	@Autowired
	QuizzService quizzService;
	
	@Autowired
	private GradeQuizService gradeQuizService;
	
	@RequestMapping(value = "/get-history-page", method = RequestMethod.GET)
	public String getHistoryPage(Model model, @RequestParam(defaultValue = "") String username, HttpSession session) {
		String token = (String) session.getAttribute("token");
		
		GradeQuizResponse gradeQuizResponse = gradeQuizService.getAllGradeQuizOfUser(username, token);
		model.addAttribute("gradequizes",gradeQuizResponse.getListGradeQuiz().getGradeQuizes());
		
		LoginedUserResponse loginedUser = quizzService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		
		return "history";
	}
	
	@RequestMapping(value = "/get-grade-quiz-of-user", method = RequestMethod.GET)
	public @ResponseBody GradeQuizResponse getAllGradeQuizOfUser(HttpSession session) {
		String token = (String) session.getAttribute("token");
		String username = "";
		GradeQuizResponse gradeQuizResponse = gradeQuizService.getAllGradeQuizOfUser(username,token);
		return gradeQuizResponse;
	}
}
