package group2.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import group2.request.QuestRequest;
import group2.request.QuestionListRequest;
import group2.request.UpdateQuestRequest;
import group2.response.LoginedUserResponse;
import group2.response.QuestResponse;
import group2.response.QuestionResponse;
import group2.response.QuizzAddResponse;
import group2.response.ResultResponse;
import group2.response.TotalAndNameResponse;
import group2.service.QuestService;
import group2.service.QuizzService;

@Controller
public class QuestionController {

	@Autowired
	private QuestService questSerice;
	
	@Autowired
	private QuizzService quizService;

	@RequestMapping(value = "/quest", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getCreateQuest(Model model,
			@RequestParam(name = "quiz_name", required = false, defaultValue = "No name") String quizName,
			HttpSession session) {
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		model.addAttribute("quizname", quizName);
		return "createanswer";
	}

	@RequestMapping(value = "/quest/add", method = RequestMethod.POST)
	public @ResponseBody QuizzAddResponse createQuest(@RequestBody QuestRequest wrapper,HttpSession session) {
		QuizzAddResponse response = new QuizzAddResponse();
		String token = (String) session.getAttribute("token");
		response.setStatus(questSerice.createQuest(wrapper,token));
		return response;
	}

	@RequestMapping(value = "/quest/update", method = RequestMethod.POST)
	public @ResponseBody QuizzAddResponse updateQuest(@RequestBody UpdateQuestRequest wrapper,HttpSession session) {
		QuizzAddResponse response = new QuizzAddResponse();
		String token = (String) session.getAttribute("token");
		response.setStatus(questSerice.updateQuest(wrapper,token) ? 1 : 2);
		return response;
	}


	@RequestMapping(value = "/quest-detail", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getDetailQuest(Model model,
			HttpSession session) {

		QuestResponse quest = new QuestResponse();
		quest.setId(4);
		quest.setType(3);
		quest.setContentQuest("q1q1q1");
		quest.setContentQuest2("q2q2");
		quest.setAnswers("aaa");
		model.addAttribute("quest", quest);
		String quizName = "QuizHoa";
		model.addAttribute("quizname", quizName);
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		return "updateAnswer";
	}

	@RequestMapping(value = "/result", method = RequestMethod.POST, produces = "text/html; charset=utf-8")
	public String getResultDoQuiz(Model model, @ModelAttribute("questRequest") QuestionListRequest quiz, BindingResult bindingResult,
			@RequestParam(defaultValue = "0") int quizId, HttpServletRequest request, HttpSession session) {
		String token = (String) session.getAttribute("token");
		String userName = (String) session.getAttribute("userName");

		ResultResponse response = questSerice.getResultQuestion(quiz, quizId, token, userName);
		if(response == null) {
			model.addAttribute("result", new ResultResponse());
		}else {
			model.addAttribute("result", response);
		}
		model.addAttribute("idQuiz", quizId);
		return "doQuiz/result";
	}

	@RequestMapping(value = "/do-quiz", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getQuestForDoQuiz(Model model, QuestionListRequest questRequest,
			@RequestParam(defaultValue = "0") int quizId, HttpSession session) {
		String token = (String) session.getAttribute("token");
		
		List<QuestionResponse> questResponse = questSerice.getAllQuestFromQuiz(quizId, token);
		if(questResponse == null) {
			model.addAttribute("totalQuestion", 0);
			model.addAttribute("nameQuiz", "");
		}else {
			model.addAttribute("totalQuestion", questResponse.size());
			model.addAttribute("nameQuiz", questResponse.get(0).getQuiz().getName());
		}
		model.addAttribute("questRequest", questRequest);
		model.addAttribute("questResponse", questResponse);
		model.addAttribute("idQuiz", quizId);
		return "doQuiz/action";
	}

	@RequestMapping(value = "/prepare-quiz", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getPrepareQuiz(Model model, @RequestParam(defaultValue = "0") int quizId, HttpSession session) {
		String token = (String) session.getAttribute("token");

		TotalAndNameResponse quest = questSerice.getTotalQuestionAndQuizName(quizId, token);
		if(quest == null) {
			model.addAttribute("nameQuiz", "");
			model.addAttribute("totalQuestion", 0);
		}else {
			model.addAttribute("nameQuiz", quest.getName());
			model.addAttribute("totalQuestion", quest.getTotal());
		}
		model.addAttribute("idQuiz", quizId);
		return "doQuiz/totalQuestion";
	}

	@RequestMapping(value = "/{id}/detail", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public String getDetailQuest(Model model, @PathVariable("id") int questId,
			@RequestParam(name = "quizz_name", required = false, defaultValue = "NoNameFake") String quizzName,
			HttpSession session) {

		QuestResponse quest = new QuestResponse();
		quest = questSerice.getGetDetail(questId);

		model.addAttribute("quest", quest);
		model.addAttribute("quizname", quizzName);
		String token = (String) session.getAttribute("token");
		LoginedUserResponse loginedUser = quizService.getLoginedUser(token);
		model.addAttribute("loginedUser",loginedUser.getUser());
		return "updateAnswer";
	}
	
	@RequestMapping(value = "/quest/{id}/delete", method = RequestMethod.DELETE)
	public @ResponseBody QuizzAddResponse deleteQuest(@PathVariable(name="id") int id,HttpSession session) {
		String token = (String) session.getAttribute("token");
		QuizzAddResponse response = new QuizzAddResponse();
		response.setStatus(questSerice.deleteQuest(id,token)? 1 : 0);
		return response;
	}
}
