package group2.dto;

import java.util.List;

import lombok.Data;

//@Data
public class QuizzList {
	private List<Quizz> quizes;

	public List<Quizz> getQuizes() {
		return quizes;
	}

	public void setQuizes(List<Quizz> quizes) {
		this.quizes = quizes;
	}

	@Override
	public String toString() {
		return "QuizzList [quizes=" + quizes + "]";
	}
}
