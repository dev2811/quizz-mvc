package group2.dto;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

//@Data
//@ToString
public class Quizz {
	private int id;
	private String name;
	private boolean status;
	private Date createdDate;
	private Date updatedDate;
	private String tag;
	private String image;
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isStatus() {
		return status;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
	
	public String getTag() {
		return tag;
	}
	
	public String getImage() {
		return image;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "Quizz [id=" + id + ", name=" + name + ", status=" + status + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + ", tag=" + tag + ", image=" + image + "]";
	}
}
