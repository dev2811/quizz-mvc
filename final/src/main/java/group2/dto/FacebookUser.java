package group2.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FacebookUser {
	private String id;
	@JsonProperty("name")
	private String userName;
	private String email;
	private int status;
	private String roleName;
	@JsonProperty("first_name")
	private String firstName;
	@JsonProperty("last_name")
	private String lastName;
	private String token;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getId() {
		return id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public String getToken() {
		return token;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "FacebookUserResponse [id=" + id + ", userName=" + userName + ", email=" + email + ", status=" + status
				+ ", roleName=" + roleName + ", token=" + token + "]";
	}
}
