package group2.dto;

import lombok.Data;

//@Data
public class User {
	private int id;
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
	private double currentScores;
	private int status;
	private String roleName;
	
	public int getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public double getCurrentScores() {
		return currentScores;
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getRoleName() {
		return roleName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCurrentScores(double currentScores) {
		this.currentScores = currentScores;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName
				+ ", email=" + email + ", currentScores=" + currentScores + ", status=" + status + ", roleName="
				+ roleName + "]";
	}
}
