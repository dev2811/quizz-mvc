package group2.dto;

import java.util.List;

import lombok.Data;

//@Data
public class GradeQuizz {
	private int id;
	private double score;
	private Quizz quiz;
	
	public int getId() {
		return id;
	}
	
	public double getScore() {
		return score;
	}
	
	public Quizz getQuiz() {
		return quiz;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public void setQuiz(Quizz quiz) {
		this.quiz = quiz;
	}

	@Override
	public String toString() {
		return "GradeQuizz [id=" + id + ", score=" + score + ", quiz=" + quiz + "]";
	}
}
