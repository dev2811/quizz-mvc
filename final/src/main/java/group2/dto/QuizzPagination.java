package group2.dto;

import java.util.List;

import lombok.Data;

//@Data
public class QuizzPagination {
	private List<Quizz> quizes;
	private int totalPages;
	
	public QuizzPagination() {
		super();
	}

	public QuizzPagination(List<Quizz> quizes, int totalPages) {
		super();
		this.quizes = quizes;
		this.totalPages = totalPages;
	}

	public List<Quizz> getQuizes() {
		return quizes;
	}
	
	public int getTotalPages() {
		return totalPages;
	}

	public void setQuizes(List<Quizz> quizes) {
		this.quizes = quizes;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	@Override
	public String toString() {
		return "QuizzPagination [quizes=" + quizes + ", totalPages=" + totalPages + "]";
	}
}
