package group2.dto;

import java.util.List;

public class UserPagination {
	private List<User> users;
	private int totalPages;
	
	public List<User> getUsers() {
		return users;
	}
	
	public int getTotalPages() {
		return totalPages;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
