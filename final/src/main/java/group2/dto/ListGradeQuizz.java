package group2.dto;

import java.util.List;

import lombok.Data;

//@Data
public class ListGradeQuizz {
	private List<GradeQuizz> gradeQuizes;

	public List<GradeQuizz> getGradeQuizes() {
		return gradeQuizes;
	}

	public void setGradeQuizes(List<GradeQuizz> gradeQuizes) {
		this.gradeQuizes = gradeQuizes;
	}

	@Override
	public String toString() {
		return "ListGradeQuizz [gradeQuizes=" + gradeQuizes + "]";
	}
}
