package group2.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestionResponse {
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("content")
	private String content;
	
	@JsonProperty("type")
	private int type;
	
	@JsonProperty("answers")
	private List<AnswerResponse> answers;
	
	private QuizResponse quiz;
	
	

	public QuizResponse getQuiz() {
		return quiz;
	}

	public void setQuiz(QuizResponse quiz) {
		this.quiz = quiz;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<AnswerResponse> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswerResponse> answers) {
		this.answers = answers;
	}


}
