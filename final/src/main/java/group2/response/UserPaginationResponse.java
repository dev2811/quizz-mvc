package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.UserPagination;

public class UserPaginationResponse {
	private int status;
	private String message;
	@JsonProperty("data")
	private UserPagination users;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public UserPagination getUsers() {
		return users;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setUsers(UserPagination users) {
		this.users = users;
	}
}
