package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.QuizzPagination;

//@Data
//@ToString
public class QuizzResponse {
	private String status;
	private String message;
	@JsonProperty("data")
	private QuizzPagination quizzPagination;
	
	public QuizzResponse() {
		super();
	}

	public QuizzResponse(String status, String message, QuizzPagination quizzPagination) {
		super();
		this.status = status;
		this.message = message;
		this.quizzPagination = quizzPagination;
	}

	public String getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public QuizzPagination getQuizzPagination() {
		return quizzPagination;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setQuizzPagination(QuizzPagination quizzPagination) {
		this.quizzPagination = quizzPagination;
	}

	@Override
	public String toString() {
		return "QuizzResponse [status=" + status + ", message=" + message + ", quizzPagination=" + quizzPagination
				+ "]";
	}
}
