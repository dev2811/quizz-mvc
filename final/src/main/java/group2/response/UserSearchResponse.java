package group2.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.User;

public class UserSearchResponse {
	private int status;
	private String message;
	@JsonProperty("data")
	private List<User> users;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public List<User> getUsers() {
		return users;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
