package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.User;
import lombok.Data;

//@Data
public class LoginedUserResponse {
	private int status;
	private String message;
	@JsonProperty("data")
	private User user;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public User getUser() {
		return user;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "LoginedUserResponse [status=" + status + ", message=" + message + ", user=" + user + "]";
	}
}
