package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.QuizzList;

//@Data
public class QuizzResponseWithOnlyData {
	private int status;
	private String message;
	@JsonProperty("data")
	private QuizzList quizzes;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public QuizzList getQuizzes() {
		return quizzes;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setQuizzes(QuizzList quizzes) {
		this.quizzes = quizzes;
	}

	@Override
	public String toString() {
		return "QuizzResponseWithOnlyData [status=" + status + ", message=" + message + ", quizzes=" + quizzes + "]";
	} 
}
