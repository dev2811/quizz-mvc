package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TotalAndNameResponse {
	@JsonProperty("total")
	private long total;
	
	@JsonProperty("name")
	private String name;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
