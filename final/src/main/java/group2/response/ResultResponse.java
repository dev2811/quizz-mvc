package group2.response;

public class ResultResponse {
	
	private long totalQuestion;
	private long attempt;
	private long correct;
	private double totalScore;
	private long wrong;

	
	
	
	public double getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}
	public long getWrong() {
		return wrong;
	}
	public void setWrong(long wrong) {
		this.wrong = wrong;
	}
	public long getTotalQuestion() {
		return totalQuestion;
	}
	public void setTotalQuestion(long totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	public long getAttempt() {
		return attempt;
	}
	public void setAttempt(long attempt) {
		this.attempt = attempt;
	}
	public long getCorrect() {
		return correct;
	}
	public void setCorrect(long correct) {
		this.correct = correct;
	}
	
	
}
