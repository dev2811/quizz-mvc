package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.ListGradeQuizz;
import lombok.Data;

//@Data
public class GradeQuizResponse {
	private int status;
	private String message;
	@JsonProperty("data")
	private ListGradeQuizz listGradeQuiz;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public ListGradeQuizz getListGradeQuiz() {
		return listGradeQuiz;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setListGradeQuiz(ListGradeQuizz listGradeQuiz) {
		this.listGradeQuiz = listGradeQuiz;
	}

	@Override
	public String toString() {
		return "GradeQuizResponse [status=" + status + ", message=" + message + ", listGradeQuiz=" + listGradeQuiz
				+ "]";
	}
}
