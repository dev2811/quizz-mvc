package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestGeneralResponse {

	@JsonProperty("id")
	private int id;
	@JsonProperty("content")
	private String content;
	@JsonProperty("type")
	private int type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
}
