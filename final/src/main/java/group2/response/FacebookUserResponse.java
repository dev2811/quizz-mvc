package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import group2.dto.FacebookUser;

public class FacebookUserResponse {
	private int status;
	private String message;
	@JsonProperty("data")
	private FacebookUser user;
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public FacebookUser getUser() {
		return user;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setUser(FacebookUser user) {
		this.user = user;
	}
	
	
}
