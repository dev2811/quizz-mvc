package group2.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponse {
	
	@JsonProperty("status")
	private int status;
	
	@JsonProperty("message")
    private String message;
	
	@JsonProperty("data")
    private Object data;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
    



}
