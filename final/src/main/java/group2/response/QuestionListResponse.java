package group2.response;

import java.util.List;

public class QuestionListResponse {
	private List<QuestionResponse> questions;

	public List<QuestionResponse> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionResponse> questions) {
		this.questions = questions;
	}
}
