package group2.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuestPaginationResponse {

	@JsonProperty("total")
	private int total;

	@JsonProperty("quests")
	private List<QuestGeneralResponse> quests;
	
	public QuestPaginationResponse() {
		this.quests = new ArrayList<QuestGeneralResponse>();
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<QuestGeneralResponse> getQuests() {
		return quests;
	}

	public void setQuests(List<QuestGeneralResponse> quests) {
		this.quests = quests;
	}
}
