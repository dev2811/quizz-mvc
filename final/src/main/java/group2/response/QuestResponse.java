package group2.response;

import java.util.ArrayList;
import java.util.List;

public class QuestResponse {

	private int id;
	private String contentQuest;
	private String contentQuest2;
	private String answers;
	private List<String> answersReal;
	private List<String> answersFake;
	private int type;
	
	public QuestResponse() {
		this.answersReal = new ArrayList<String>();
		this.answersFake = new ArrayList<String>();
		/*
		 * this.answersReal.add("a1"); this.answersReal.add("a2");
		 * this.answersReal.add("a3"); this.answersFake.add("af1");
		 * this.answersFake.add("af2");
		 */
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContentQuest() {
		return contentQuest;
	}

	public void setContentQuest(String contentQuets) {
		this.contentQuest = contentQuets;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
	}

	public List<String> getAnswersReal() {
		return answersReal;
	}

	public void setAnswersReal(List<String> answersReal) {
		this.answersReal = answersReal;
	}

	public List<String> getAnswersFake() {
		return answersFake;
	}

	public void setAnswersFake(List<String> answersFake) {
		this.answersFake = answersFake;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContentQuest2() {
		return contentQuest2;
	}

	public void setContentQuest2(String contentQuest2) {
		this.contentQuest2 = contentQuest2;
	}
	
	
}
