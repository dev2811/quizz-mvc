package group2.response;

public class PostResponse {

	private Integer id;

	private String picture;

	private Double countLike;

	private String avatar;

	private String title;
	
	private boolean like;
	
	private String userName;

	public PostResponse() {

	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Double getCountLike() {
		return countLike;
	}

	public void setCountLike(Double countLike) {
		this.countLike = countLike;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isLike() {
		return like;
	}

	public void setLike(boolean like) {
		this.like = like;
	}

}
