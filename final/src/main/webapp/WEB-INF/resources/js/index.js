var pageNumbers = $('footer .page-item');
var listQuiz = $('#list-quiz');
var searchBtn = $('#btn-search');
var searchInput = $('#keyword');
var footer = $('#footer');

function activeButtonPage(button){
	$('.pagination .page-item.active').removeClass('active');
    button.addClass('active');
}

function showQuiz(quizz){
	let card = `<div class="col-md-3">
                    <a href="/prepare-quiz?quizId=${quizz.id}" class="d-block card-link">
                        <div class="card">
                            <div class="img-area">
                                <img class="card-img-top" src="${quizz.image}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title text-center">${quizz.name}</h5>
                            </div>
                        </div>
                    </a>
                </div>`;
	listQuiz.append(card);
}

//pagination
$(document).ready(function(){
    pageNumbers.on("click",function(e){
        e.preventDefault();
        activeButtonPage($(this));
		
		//load list quiz by pageNumber
		let page = $(this).text();
		$.ajax({
			url:`/final/get-quizzes-by-page-number?pageNumber=${page}`,
			method:'GET'
		}).done(function(data){
			listQuiz.empty();
			data.forEach(d=>{
				showQuiz(d);
			});
		});
    });
});

//search quiz by name
searchBtn.on('click', function(e){
	e.preventDefault();
	console.log(searchInput.val()=="");
	
	if(searchInput.val()==""){
		$.ajax({
			url:'/final/get-quizzes-by-page-number',
			method:'GET'
		}).done(function(data){
			if(data!=""){
				listQuiz.empty();
				footer.addClass("d-flex");
				footer.removeClass("d-none");
				data.forEach(d=>{
					showQuiz(d);
				});
			}
		});
	}
	else{
		console.log(searchInput.val());
		let name = searchInput.val();
		$.ajax({
			url:`/final/get-quizzes-by-name?name=${name}`,
			method:'GET'
		}).done(function(data){
			if(data!=""){
				listQuiz.empty();
				footer.removeClass("d-flex");
				footer.addClass("d-none");
				data.forEach(d=>{
					showQuiz(d);
				});
			}
		});
	}
});