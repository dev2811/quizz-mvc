$(document).ready(function () {

    $.validator.addMethod(
        'matches', function (value, element, param) {
            var regex = param instanceof RegExp ? param : new RegExp(param);
            return this.optional(element) || regex.test(value);
        });

    $("#formJob").validate({
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                matches: "^\[0-9]{3}-[0-9]{3}-[0-9]{7}$"
            },
            selectCountry: {
                required: true
            },
            desiredPosition: {
                required: true
            }
        },
        messages: {
            firstName: {
                required: "The First Name should not be blank"
            },
            lastName: {
                required: "The First Name should not be blank"
            },
            email: {
                required: "The Email should not be blank",
                email: "Please input your correct email address"
            },
            phone: {
                required: "Please input your correct phone number",
                matches: "Please input your correct phone number"
            },
            selectCountry: {
                required: "Please select your country"
            },
            desiredPosition: {
                required: "Please select the position"
            }

        }
    });
});