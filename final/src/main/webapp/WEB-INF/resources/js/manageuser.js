var tbody = $("#tbody");
var searchName = $("#search-name");
var searchStatus = $("#search-status");
var pageNumbers = $('footer .page-item');
var footer = $('#footer');
var alert = $('#alert');

function activeButtonPage(button){
	$('.pagination .page-item.active').removeClass('active');
    button.addClass('active');
}

function showTableData(id, username, email, status) {
    let row = `<tr data-id=${id} id=${id} class="data">
		                <td class="font-weight-normal">${username}</td>
		                <td>${email}</td>
						${status == 0?'<td>Inactive</td>':'<td>Active</td>'}
		                <td>
							<button class="px-1 mr-1 border-0 rounded btn btn-info btn-active" data-id="${id}">Active</button>
                            <button class="px-1 border-0 rounded btn btn-danger btn-disable" data-id=${id}>Disable</button>
		                </td>
              		</tr>`;
    tbody.append(row);
}

$(document).ready(function () {
	//search name
	searchName.on("change", function () {
        let value = $(this).val().toLowerCase();

        if (value == "") {
            $.ajax({
				url:'/final/get-users-by-page-number',
				method:'GET'
			}).done(function(data){
				if(data!=null){
					$("tr").remove(".data");
					footer.addClass("d-flex");
					footer.removeClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.userName, d.email, d.status);
					});
				}
			});
        }
        else {
            $.ajax({
				url:`/final/get-users-by-username?username=${value}`,
				method:'GET'
			}).done(function(data){
				if(data!=null){
					console.log(data);
					$("tr").remove(".data");
					footer.removeClass("d-flex");
					footer.addClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.userName, d.email, d.status);
					});
				}
			});
        }
    });
	
	//search status
	searchStatus.on("change", function(){
		let value = $(this).val();
		console.log(value);
		
		if (value == "") {
            $.ajax({
				url:'/final/get-users-by-page-number',
				method:'GET'
			}).done(function(data){
				if(data!=null){
					$("tr").remove(".data");
					footer.addClass("d-flex");
					footer.removeClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.userName, d.email, d.status);
					});
				}
			});
        }
        else{
            $.ajax({
				url:`/final/get-users-by-status?status=${value}`,
				method:'GET'
			}).done(function(data){
				if(data!=null){
					console.log(data);
					$("tr").remove(".data");
					footer.removeClass("d-flex");
					footer.addClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.userName, d.email, d.status);
					});
				}
			});
        }
	});

	//pagination
	pageNumbers.on("click",function(e){
        e.preventDefault();
        activeButtonPage($(this));
		
		//load list quiz by pageNumber
		let page = $(this).text();
		$.ajax({
			url:`/final/get-users-by-page-number?pageNumber=${page}`,
			method:'GET'
		}).done(function(data){
			$("tr").remove(".data");
			data.forEach(d=>{
				showTableData(d.id, d.userName, d.email, d.status);
			});
		});
    });

	tbody.on("click",function(e){
		//disable user
		if(e.target.classList.contains('btn-disable')){
		    let rowId = $(e.target).data("id");
			
			 if (confirm('Do you want to disable this user?')) {
		        $.ajax({
		            url: `/final/disable-user/${rowId}`,
		            method: "get"
		        }).done(function (data) {
					if(data.id != 0){
						alert.removeClass('d-block');
						alert.addClass('d-none');
						$(`#${rowId}`).html(`<td class="font-weight-normal">${data.userName}</td>
							                <td>${data.email}</td>
											${data.status == 0?'<td>Inactive</td>':'<td>Active</td>'}
							                <td>
												<button class="px-1 mr-1 border-0 rounded btn btn-info btn-active" data-id="${data.id}">Active</button>
					                            <button class="px-1 border-0 rounded btn btn-danger btn-disable" data-id=${data.id}>Disable</button>
							                </td>`);
					}
					else{
						alert.removeClass('d-none');
						alert.addClass('d-block');
						alert.html('Cannot disable this user!');
					}
				});
			}
		}
		
		//active user
		if(e.target.classList.contains('btn-active')){
		    let rowId = $(e.target).data("id");
			
			 if (confirm('Do you want to activate this user?')) {
		        $.ajax({
		            url: `/final/activate-user/${rowId}`,
		            method: "get"
		        }).done(function (data) {
					if(data.id != 0){
						alert.removeClass('d-block');
						alert.addClass('d-none');
						$(`#${rowId}`).html(`<td class="font-weight-normal">${data.userName}</td>
							                <td>${data.email}</td>
											${data.status == 0?'<td>Inactive</td>':'<td>Active</td>'}
							                <td>
												<button class="px-1 mr-1 border-0 rounded btn btn-info btn-active" data-id="${data.id}">Active</button>
					                            <button class="px-1 border-0 rounded btn btn-danger btn-disable" data-id=${data.id}>Disable</button>
							                </td>`);
					}
					else{
						alert.removeClass('d-none');
						alert.addClass('d-block');
						alert.html('Cannot activate this user!');
					}
				});
			}
		}
	});
});

