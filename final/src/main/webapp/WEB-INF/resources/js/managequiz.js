var tbody = $("#tbody");
var searchName = $("#search-name");	
var pageNumbers = $('footer .page-item');
var footer = $('#footer');
var addFooter = `<div th:replace="layout/footer.html :: footer"></div>`;

function activeButtonPage(button){
	$('.pagination .page-item.active').removeClass('active');
    button.addClass('active');
}

function showTableData(id, title, createDate, updateDate) {
	//format date
	let createDateString = "";
	let updateDateString = "";
	if(createDate != null){
		let createdDate = new Date(createDate);
		let updatedDate = new Date(updateDate);
		//console.log(createdDate);
		//console.log(createDate);
		
		createDateString = createdDate.toLocaleDateString('en-GB');
	}
	
	if(updateDate != null){
		let updatedDate = new Date(updateDate);
		//console.log(updatedDate);
		//console.log(updateDate);
		
		updateDateString = updatedDate.toLocaleDateString('en-GB');
	}
	
	//add row
    let row = `<tr data-id=${id} id=${id} class="data">

		                <td><a href=/final/${id}/get-all-quest>${title}</a></td>

		                <td>${createDateString}</td>
						<td>${updateDateString}</td>
		                <td>
                            <button class="px-1 border-0 rounded bg-danger text-white btn-delete" data-id=${id}>Delete</button>
		                </td>
              		</tr>`;
    tbody.append(row);
}

$(document).ready(function () {
    //search name
	searchName.on("change", function () {
        let value = $(this).val().toLowerCase();

        if (value == "") {
            $.ajax({
				url:'/final/get-quizzes-by-page-number',
				method:'GET'
			}).done(function(data){
				if(data!=null){
					$("tr").remove(".data");
					footer.addClass("d-flex");
					footer.removeClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.name, d.createdDate, d.updatedDate);
					});
				}
			});
        }
        else {
            $.ajax({
				url:`/final/get-quizzes-by-name?name=${value}`,
				method:'GET'
			}).done(function(data){
				if(data!=null){
					console.log(data);
					$("tr").remove(".data");
					footer.removeClass("d-flex");
					footer.addClass("d-none");
					data.forEach(d=>{
						showTableData(d.id, d.name, d.createdDate, d.updatedDate);
					});
				}
			});
        }
    });
    

	//pagination
    pageNumbers.on("click",function(e){
        e.preventDefault();
        activeButtonPage($(this));
		
		//load list quiz by pageNumber
		let page = $(this).text();
		$.ajax({
			url:`/final/get-quizzes-by-page-number?pageNumber=${page}`,
			method:'GET'
		}).done(function(data){
			$("tr").remove(".data");
			data.forEach(d=>{
				showTableData(d.id, d.name, d.createdDate, d.updatedDate);
			});
		});
    });

	//delete post
	tbody.on("click",function(e){
		if(e.target.classList.contains('btn-delete')){
			let alert = $('#alert');
		    let rowId = $(e.target).data("id");

		    if (confirm('Do you want to delete this?')) {
		        $.ajax({
		            url: `/final/delete-quiz/${rowId}`,
		            method: "delete"
		        }).done(function (data) {
					alert.addClass('d-none');
					alert.removeClass('d-block');
					alert.removeClass('');
					
					$(`#${rowId}`).remove();
		        }).fail(function(data){
					if(data.status == 404){
						alert.removeClass('d-none');
						alert.addClass('d-block');
						alert.html('Quiz does not exist.');
					}
				});
		    }
		}
	});
});

