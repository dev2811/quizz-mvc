var tbody = $("#tbody");
var searchName = $("#search-name");
var searchStatus = $("#search-status");

function showTableData(id, username, email, status) {
    let row = `<tr data-id=${id} id=${id} class="data">
		                <td class="font-weight-normal">${username}</td>
		                <td>${email}</td>
                        <td>${status}</td>
		                <td>
                            <button class="px-1 border-0 rounded btn btn-danger btn-disable">Disable</button>
		                </td>
              		</tr>`;
    tbody.append(row);
}

$(document).ready(function () {
    var array = [];
    var headers = [];

    headers[0] = 'id';
    $('#list-user-table th:not(:last-child)').each(function (index, item) {
        headers[index + 1] = $(item).html().toLowerCase().replace(" ", "");
    });
    $('#list-user-table tr:not(:first-child)').has('td').each(function (index, it) {
        var arrayItem = {};
        arrayItem[headers[0]] = $(it).data('id');
        $('td:not(:last-child)', $(this)).each(function (index, item) {
            if ($(item).children().length != 0) {
                arrayItem[headers[index + 1]] = $(item).children().first().html();
            }
            else {
                arrayItem[headers[index + 1]] = $(item).html();
            }
        });
        array.push(arrayItem);
        console.log(array);
    });

    //search status
    searchStatus.on("change", function () {
        let value = $(this).children(':selected').text();
        console.log(value);

        if (value != "All") {
            $("tr").remove(".data");
            array.filter(a => a.status == value).forEach(a => showTableData(a.id, a.username, a.email, a.status));
        }
        else {
            $("tr").remove(".data");
            array.forEach(a => showTableData(a.id, a.username, a.email, a.status));
        }
    });

    //search name
    searchName.on("change", function () {
        let value = $(this).val().toLowerCase();

        if (value != "" && value != null) {
            $("tr").remove(".data");
            array.filter(a => a.username.toLowerCase().includes(value)).forEach(a => showTableData(a.id, a.username, a.email, a.status));
        }
        else {
            $("tr").remove(".data");
            array.forEach(a => showTableData(a.id, a.username, a.email, a.status));
        }
    });
});

//disable
var btnDisable = $(".btn-disable");
btnDisable.on("click", function () {
    let rowId = $(this).data("id");
    if (confirm('Do you want to delete this?')) {
        $.ajax({
            url: `http://localhost:8080/JSFW.L.A102/blog/delete-blog?id=${rowId}`,
            method: "get"
        }).done(function () {
            $(`#${rowId}`).remove();
        });
        console.log($(`#${rowId}`));
    }
});